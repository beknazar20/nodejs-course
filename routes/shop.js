const express = require('express')
const path = require('path')
const router = express.Router();
const rootDir = require('../util/path')
const adminData = require('./admin')
router.get('/',(req,res)=>{
    console.log(adminData.products)
    res.sendFile(path.join(rootDir, 'views','shop.html'))
    
    // res.send('<H1>Hello Express APP</H1>')
});

module.exports = router;