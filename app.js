const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()

const adminData = require('./routes/admin')
const shopRoutes = require('./routes/shop')
app.use(bodyParser.urlencoded({extended:true}))
app.use('/admin',adminData.routes)
app.use(shopRoutes)
app.use(express.static('public'))
app.use(express.static(path.join(__dirname,'public')))
app.use((req,res)=>{
    // res.status(404).send('<h1>Page not found!</h1>')
    res.status(494).sendFile(path.join(__dirname,'views','404.html'))
})
app.listen(3000,()=>{
    console.log('Server started on port 3000')
})